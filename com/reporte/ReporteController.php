<?php
/**
 * Controlador para el recurso Reporte
 */
namespace com\reporte;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso Reporte, atiende a las peticiones que contengas /mvc/reporte
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @cateogry 	Controller
 * @package 	Boveda
 * @subpackage 	Reporte
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/reporte})
 */
class ReporteController
{
	/** @Resource(name=ReporteService) */
	protected $reporteService;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso reporte/descargar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function descargarAction($idEmpresas)
	{
		$this->logger->info("Atendiendo la peticion /reporte/decargar");
		$response = $this->reporteService->descargar(json_decode($idEmpresas, true));
		$this->response($response);
	}
}
