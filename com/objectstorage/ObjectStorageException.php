<?php
/**
 * Excepciones genericas para el componente ObjectStorage
 */
namespace com\objectstorage;

use Exception;


/**
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category 	Exception
 * @package 	Boveda
 * @subpackage 	ObjectStorage
 * @version 	1.0
 *
 * Excepciones genericas para el componente ObjectStorage
 */
class ObjectStorageException extends Exception
{

}