<?php
/**
 * Servicio para el componente Upload
 */
namespace com\objectstorage\upload;

use MNIComponents\Base\TService;
use com\objectstorage\ObjectStorageException;

/** 
 * @author		Israel Hernández <iaejean@hotmail.com>
 * @category 	Service
 * @package 	Boveda
 * @subpackage 	Upload
 * @version 	1.0 
 *
 * Servicio para el componente Upload
 * 
 * @Component(name=UploadService)
 * @Singleton
 */
class UploadService  
{	
	protected $logger;
	use TService;

	public function validarUpload(Upload $upload, $typeRequired)
	{
		$this->logger->info("Servicio para la validacion correcta de un archivo subido, extensio, integridad..");

		$type = array();
		switch($typeRequired){
			case "xml": array_push($type, "text/xml"); break;
			case "zip": array_push($type, "application/stream", "application/x-zip-compressed", "application/octet-stream"); break;
			case "ordenCompra": array_push($type, "application/pdf"); break;
			case "representacionGrafica": array_push($type, "application/pdf"); break;
			default: break;
		}

		$error = $upload->getError();
		if(!empty($error)){
			switch($error){
				case '1': throw new ObjectStorageException("Archivo excede del maximo permitido: ".ini_get("upload_max_filesize")); break;
				case '2': throw new ObjectStorageException("Archivo excede del maximo permitido por el formulario: ".ini_get("MAX_FILE_SIZE")); break;
				case '3': throw new ObjectStorageException("El archivo fue subido parcialmente"); break;
				case '4': throw new ObjectStorageException("El archivo no fue subido"); break;
				case '6': throw new ObjectStorageException("Problemas al generar el archivo temporal"); break;
				case '7': throw new ObjectStorageException("Problemas al escribir el archivo"); break;
				case '8': throw new ObjectStorageException("Se detuvo la subida por la extension del archivo"); break;
				case '999': default: throw new ObjectStorageException("Ha ocurrido un error desconocido al subir el archivo"); break;
			}
		}
		//if(!preg_match("/^[a-zA-Z0-9 .-_()]*$/", $upload->getName()))
			//throw new ObjectStorageException("El archivo contiene caracteres especiales en su nombre, favor de eliminarlos.");			
		if(!in_array($upload->getType(), $type))
			throw new ObjectStorageException("El archivo no fue el esperado, favor de subir un archivo válido");
		return true;
	}	
}
