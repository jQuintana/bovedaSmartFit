<?php
/**
 * Clase entidad de Usuario
 */
namespace com\usuario;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Usuario
 * @version 	1.1
 *
 * @Component(name=Usuario)
 * @Prototype
 */
class Usuario extends BModel
{
	private $usuario;
	private $password;
	private $nombre;
	private $apellidoPaterno;
	private $apellidoMaterno;
	private $correoElectronico;
	private $estatus;
	use TModel;

	public function getUsuario()
	{
		return $this->usuario;
	}

	public function setUsuario($usuario)
	{
		$this->usuario = $usuario;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function setPassword($password)
	{
		$this->password = $password;
	}

	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}

	public function getNombre()
	{
		return $this->nombre;
	}

	public function getApellidoPaterno()
	{
		return $this->apellidoPaterno;
	}

	public function setApellidoPaterno($apellidoPaterno)
	{
		$this->apellidoPaterno = $apellidoPaterno;
	}

	public function getApellidoMaterno()
	{
		return $this->apellidoMaterno;
	}

	public function setApellidoMaterno($apellidoMaterno)
	{
		$this->apellidoMaterno = $apellidoMaterno;
	}

	public function getCorreoElectronico()
	{
		return $this->correoElectronico;
	}

	public function setCorreoElectronico($correoElectronico)
	{
		$this->correoElectronico = $correoElectronico;
	}

	public function getEstatus()
	{
		return $this->estatus;
	}

	public function setEstatus($estatus)
	{
		$this->estatus = $estatus;
	}
}