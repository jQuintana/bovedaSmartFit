<?php
/**
 * DAO's del recurso Usuario
 */
namespace com\usuario;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso Usuario
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAP
 * @package 	Boveda
 * @subpackage 	Usuario
 * @version 	1.1
 * 
 * @Component(name=UsuarioDao)
 * @Singleton
 */
class UsuarioDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
