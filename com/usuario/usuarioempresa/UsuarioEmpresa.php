<?php
/**
 * Clase entidad de Usuario Empresa
 */
namespace com\usuario\usuarioEmpresa;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	UsuarioEmpresa
 * @version 	1.1
 *
 * @Component(name=UsuarioEmpresa)
 * @Prototype
 */
class UsuarioEmpresa extends BModel
{
	private $idUsuario;
	private $idEmpresa;
	use TModel;

	public function getIdUsuario()
	{
		return $this->idUsuario;
	}

	public function setIdUsuario($idUsuario)
	{
		$this->idUsuario = $idUsuario;
	}

	public function getIdEmpresa()
	{
		return $this->idEmpresa;
	}

	public function setIdEmpresa($idEmpresa)
	{
		$this->idEmpresa = $idEmpresa;
	}	
}