<?php
/**
 * Service para el recurso Usuario
 */
namespace com\usuario;

use MNIComponents\Base\TService;
use com\sesion\Sesion;
use Exception;


/**
 * Service para el recurso Usuario
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Usuario
 * @version 	1.1
 *
 * @Component(name=UsuarioService)
 * @Singleton
 */
class UsuarioService 
{
	/** @Resource(name=UsuarioDao) */ 
	protected $usuarioDao;
	/** @Resource(name=Sesion) */ 
	protected $sesion;	
	/** @Resource(name=SesionService) */ 
	protected $sesionService;
	/** @Resource(name=ACLService) */ 
	protected $aclService;

	/** @Resource(name=UsuarioEmpresa) */
	protected $usuarioEmpresa;
	/** @Resource(name=UsuarioEmpresaService) */
	protected $usuarioEmpresaService;

	protected $logger;
	use TService;

	/**
	 * Valida que el usuario que intenta logearse se válido, además solicita los servicios de la 
	 * sesion para crear/actualizar/validar el token del usuario
	 * @param Usuario $usuario
	 * @return array
	 * @throws Exception
	 */
	public function validar(Usuario $usuario)
	{
		if($usuario->getUsuario() == "" || $usuario->getPassword() == "")
			throw new Exception("Favor de ingresar usuario y contraseña");
		$listUsuarios = $this->usuarioDao->consultar($usuario);
		if(empty($listUsuarios))
			throw new Exception("Favor de ingresar usuario y contraseña válidos");		
		$usuarioArray = $listUsuarios[0];
		if(md5($usuario->getPassword()) !== $usuarioArray["password"])
			throw new Exception("Favor de ingresar usuario y contraseña válidos");
		if($usuarioArray["estatus"] != 1 && $usuarioArray["estatus"] != 3 )
			throw new Exception("Favor de ingresar usuario y contraseña válidos");

		$this->sesion->setIdUsuario($usuario->getUsuario());
		$sesionArray = $this->sesionService->token($this->sesion);
		return array($usuarioArray, $sesionArray);
	}

	/**
	 * Finaliza la sesion de un usuario logeado en el sistema desde el menu del portal de 
	 * administracion
	 * @param Usuario $usuario
	 * @return boolean
	 */
	public function logout(Usuario $usuario)
	{
		$this->sesion->setIdUsuario($usuario->getUsuario());
		$this->sesionService->eliminar($this->sesion);		
		return true;
	}

	/**
	 * Genera un listado de usuario existentes
	 * @param Usuario $usuario
	 * @return array
	 */
	public function listar(Usuario $usuario)
	{
		return $this->usuarioDao->listar($usuario);
	}

	/**
	 * Permite eliminar usuarios
	 * @param Usuario $usuario
	 * @param Sesion $sesion
	 * @return array	 
	 */
	public function eliminar(Usuario $usuario, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$this->usuarioEmpresa->setIdUsuario($usuario->getUsuario());
			$this->usuarioEmpresaService->eliminar($this->usuarioEmpresa, $sesion);
			$this->usuarioDao->eliminar($usuario);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Permite crear un usuario nuevo
	 * @param Usuario $usuario
	 * @param Sesion $sesion
	 * @return array
	 */
	public function insertar(Usuario $usuario, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->usuarioDao->insertar($usuario);
			return $this->sesionService->token($sesion);
		}
	}

	/**
	 * Permite actualizar la informacion de un usuario
	 * @param Usuario $usuario
	 * @param Sesion $sesion
	 * @return array
	 */
	public function actualizar(Usuario $usuario, Sesion $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$list = $this->usuarioDao->actualizar($usuario);
			return $this->sesionService->token($sesion);
		}
	}
	
	/**
	 * Consulta un usuario deyerminado
	 * @param Usuario $usuario
	 * @return array
	 */
	public function consultar(Usuario $usuario)
	{
		return $this->usuarioDao->consultar($usuario);
	}
}
