<?php
/**
 * Controller para el recurso Usuario
 */
namespace com\usuario;

use MNIComponents\Base\TController;


/**
 * Controller del recurso usuario, atiende a las peticiones que contengas /mvc/usuario
 *
 * @author 		Israel Hernández
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Usuario
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/usuario})
 */
class UsuarioController
{
	/** @Resource(name=Usuario) */
	protected $usuario;
	/** @Resource(name=UsuarioService) */
	protected $usuarioService;
	/** @Resource(name=Sesion) */	
	protected $sesion;
	protected $jqgridfilter;
	protected $logger;	
	use TController;

	/**
	 * Atiende peticiones al recurso usuario/login
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function loginAction($usuario)
	{
		$this->logger->info("Atendiendo la peticion /usuario/login");		
		$this->usuario->__fromJson($usuario);
		$response = $this->usuarioService->validar($this->usuario);
		$this->response($response);		
	}

	/**
	 * Atiende peticiones al recurso usuario/logout
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function logoutAction($usuario)
	{
		$this->logger->info("Atendiendo la peticion /usuario/logout");
		$this->usuario->__fromJson($usuario);
		$response = $this->usuarioService->logout($this->usuario);
		$this->response($response);		
	}

	/**
	 * Atiende peticiones al recurso usuario/listar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function listarAction()
	{
		$this->logger->info("Atendiendo la peticion /usuario/listar");
		$response = $this->usuarioService->listar($this->usuario);
		$this->response($response);		
	}

	/**
	 * Atiende peticiones al recurso usuario/eliminar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function eliminarAction($usuario, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /usuario/eliminar");		
		$this->usuario->__fromJson($usuario);
		$this->sesion->__fromJson($sesion);
		$response = $this->usuarioService->eliminar($this->usuario, $this->sesion);
		$this->response($response);		
	}

	/**
	 * Atiende peticiones al recurso usuario/insertar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function insertarAction($usuario, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /usuario/insertar");
		$this->usuario->__fromJson($usuario);
		$this->sesion->__fromJson($sesion);		
		$response = $this->usuarioService->insertar($this->usuario, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso usuario/actualizar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function actualizarAction($usuario, $idUsuario = null, $sesion = null)
	{
		$this->logger->info("Atendiendo la peticion /usuario/actualizar");
		$sesion = (isset($idUsuario)) ? $idUsuario : $sesion ;
		$this->usuario->__fromJson($usuario);
		$this->sesion->__fromJson($sesion);		
		$response = $this->usuarioService->actualizar($this->usuario, $this->sesion);
		$this->response($response);
	}
	
	/**
	 * Atiende peticiones al recurso usuario/consultar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function consultarAction($usuario)
	{
		$this->logger->info("Atendiendo la peticion /usuario/login");
		$this->usuario->__fromJson($usuario);
		$response = $this->usuarioService->consultar($this->usuario);
		$this->response($response);	
	}
}
