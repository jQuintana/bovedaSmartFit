<?php
/**
 * Modelo de la clase Recurso
 */
namespace com\sacl\recurso;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Recurso
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=Recurso)
 * @Prototype
 */
class Recurso extends BModel
{
	private $idRecurso;
	private $descipcion;
	use TModel;

	public function getIdRecurso()
	{
		return $this->idRecurso;
	}
	
	public function setIdRecurso($idRecurso)
	{
		$this->idRecurso = $idRecurso;
	}
	
	public function getDescripcion()
	{
		return $this->descripcion;
	}
	
	public function setDescripcion($descripcion)
	{
		$this->descripcion = $descripcion;
	}
}
