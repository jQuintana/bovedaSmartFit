<?php
/**
 * DAO's del recurso Opcion
 */
namespace com\sacl\opcion;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso Opcion
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=OpcionDao)
 * @Singleton 
 */
class OpcionDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
}
