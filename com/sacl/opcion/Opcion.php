<?php
/**
 * Modelo de la clase Opcion
 */
namespace com\sacl\opcion;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Opcion
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=Opcion)
 * @Prototype
 */
class Opcion extends BModel
{
	private $idOpcion;
	private $alias;
	private $url;
	private $menu;
	private $class;
	use TModel;

	public function getIdOpcion()
	{
		return $this->idOpcion ;
	}
	
	public function setIdOpcion($idOpcion)
	{
		$this->idOpcion = $idOpcion;
	}
	
	public function getAlias()
	{
		return $this->alias ;
	}
	
	public function setAlias($alias)
	{
		$this->alias = $alias;
	}
	
	public function getUrl()
	{
		return $this->url ;
	}
	
	public function setUrl($url)
	{
		$this->url = $url;
	}
	
	public function getMenu()
	{
		return $this->menu ;
	}
	
	public function setMenu($menu)
	{
		$this->menu = $menu;
	}
	
	public function getClass()
	{
		return $this->class ;
	}
	
	public function setClass($class)
	{
		$this->class = $class;
	}
}
