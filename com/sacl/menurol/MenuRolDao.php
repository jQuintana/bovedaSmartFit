<?php
/**
 * DAO de la clase MenuRol
 */
namespace com\sacl\menurol;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's del recurso MenuRol
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=MenuRolDao)
 * @Singleton
 */
class MenuRolDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
