<?php
/**
 * Servicios del componente Rol
 */
namespace com\sacl\rol;

use MNIComponents\Base\TService;


/**
 * Servicios del componente Rol
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category 	Service
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 * 
 * @Component(name=RolService)
 * @Singleton
 */
class RolService
{
	/** @Resource(name=RolDao) */
	protected $rolDao;
	protected $logger;
	use TService;

	/**
	 * Listalos roles existentes
	 * @param \com\sacl\Rol $rol
	 * @return array
	 */
	public function listar($rol)
	{
		return $this->rolDao->listar($rol);
	}
}
