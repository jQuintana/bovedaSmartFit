<?php
/**
 * Modelo del recurso ACL
 */
namespace com\sacl;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo del recurso ACL
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	ACL
 * @version 	1.1
 *
 * @Component(name=ACL)
 * @Prototype
 */
class ACL extends BModel
{
	private $recurso;
	private $rol;
	private $permiso;
	use TModel;

	public function getRol()
	{
		return $this->rol;
	}
	
	public function setRol($rol)
	{
		$this->rol = $rol;
	}
	
	public function getRecurso()
	{
		return $this->recurso;
	}
	
	public function setRecurso($recurso)
	{
		$this->recurso = $recurso;
	}
	
	public function getPermiso()
	{
		return $this->permiso;
	}
	
	public function setPermiso($permiso)
	{
		$this->permiso = $permiso;
	}
}
