<?php
/**
 * Controlador para el recurso Proveedor
 */
namespace com\proveedor;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso proveedor, atiende a las peticiones que contengas /mvc/proveedor
 *
 * @author 		Israel Hern�ndez
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/proveedor})
 */
class ProveedorController
{
	/** @Resource(name=Proveedor) */
	protected $proveedor;
	/** @Resource(name=ProveedorService) */
	protected $proveedorService;
	/** @Resource(name=Sesion) */
	protected $sesion;	
	protected $logger;	
	use TController;

	/**
	 * Atiende peticiones al recurso proveedor/listar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarAction(){
		$this->logger->info("Atendiendo la peticion /proveedor/listar");		
		$response = $this->proveedorService->listar($this->proveedor);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso proveedor/insertar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function insertarAction($proveedor, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /proveedor/insertar");
		$this->proveedor->__fromJson($proveedor);
		$this->sesion->__fromJson($sesion);
		$response = $this->proveedorService->insertar($this->proveedor, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso proveedor/actualizar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function actualizarAction($proveedor, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /proveedor/actualizar");
		$this->proveedor->__fromJson($proveedor);
		$this->sesion->__fromJson($sesion);
		$response = $this->proveedorService->actualizar($this->proveedor, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso proveedor/eliminar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function eliminarAction($proveedor, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /proveedor/eliminar");
		$this->proveedor->__fromJson($proveedor);
		$this->sesion->__fromJson($sesion);
		$response = $this->proveedorService->eliminar($this->proveedor, $this->sesion);
		$this->response($response);
	}
}
