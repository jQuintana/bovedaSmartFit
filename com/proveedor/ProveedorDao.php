<?php
/**
 * DAO's para el recurso Proveedor
 */
namespace com\proveedor;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso Proveedor
 *
 * @author 		Israel Hernándex <iaejean@hotmail.com>
 * @category 	DAO
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 * 
 * @Component(name=ProveedorDao)
 * @Singleton
 */
class ProveedorDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;	
}
