<?php
/**
 * Modelo de la clase InformacionRequerida
 */
namespace com\proveedor\informacionrequerida;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase InformacionRequerida
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model 
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Component(name=InformacionRequerida)
 * @Prototype
 */
class InformacionRequerida extends BModel
{
	private $idInformacionRequerida;
	private $valor;
	use TModel;

	public function getValor()
	{
		return $this->valor ;
	}
	
	public function setValor($valor)
	{
		$this->valor = $valor;
	}
	
	public function getIdInformacionRequerida()
	{
		return $this->idInformacionRequerida ;
	}
	
	public function setIdInformacionRequerida($idInformacionRequerida)
	{
		$this->idInformacionRequerida = $idInformacionRequerida;
	}
}
