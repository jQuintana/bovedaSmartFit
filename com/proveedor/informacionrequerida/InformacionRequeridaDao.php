<?php
/**
 * DAO's para el recurso InformacionRequerida
 */
namespace com\proveedor\informacionrequerida;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso InformacionRequerida
 *
 * @author 		Israel Hernándex
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 * 
 * @Component(name=InformacionRequeridaDao)
 * @Singleton
 */
class InformacionRequeridaDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
}

