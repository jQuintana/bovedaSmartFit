<?php
/**
 * Modelo de la clase Proveedor
 */
namespace com\proveedor;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Proveedor
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Component(name=Proveedor)
 * @Prototype
 */
class Proveedor extends BModel
{
	private $idProveedor;
	private $nombre;
	private $rfc;
	private $estatus;
	use TModel;

	public function getIdProveedor()
	{		
		return $this->idProveedor ;
	}
	
	public function setIdProveedor($idProveedor)
	{
		$this->idProveedor = $idProveedor;
	}
	
	public function getNombre()
	{
		return $this->nombre ;
	}
	
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}
	
	public function getRfc()
	{
		return $this->rfc ;
	}
	
	public function setRfc($rfc)
	{
		$this->rfc = $rfc;
	}
	
	public function getEstatus()
	{
		return $this->estatus ;
	}
	
	public function setEstatus($estatus)
	{
		$this->estatus = $estatus;
	}
}
