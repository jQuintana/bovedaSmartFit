<?php
/**
 * DAO's para el recurso ProveedorInformacionRequerida
 */
namespace com\proveedor;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso ProveedorInformacionRequerida
 *
 * @author 		Israel Hernándex <iaejean@hotmail.com>
 * @category 	DAO
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 * 
 * @Component(name=ProveedorInformacionRequeridaDao)
 * @Singleton
 */
class ProveedorInformacionRequeridaDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
}
