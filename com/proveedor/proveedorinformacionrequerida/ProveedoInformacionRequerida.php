<?php
/**
 * Modelo de la clase ProveedorInformacionRequerida
 */
namespace com\proveedor;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase ProveedorInformacionRequerida
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Component(name=ProveedorInformacionRequerida)
 * @Prototype
 */		
class ProveedorInformacionRequerida extends BModel
{
	private $idProveedor;
	private $idInformacionRequerida;
	use TModel;
	
	public function getIdProveedor()
	{
		return $this->idProveedor ;
	}
	
	public function setIdProveedor($idProveedor)
	{
		$this->idProveedor = $idProveedor;
	}
	
	public function getIdInformacionRequerida()
	{
		return $this->idInformacionRequerida ;
	}
	
	public function setIdInformacionRequerida($idInformacionRequerida)
	{
		$this->idInformacionRequerida = $idInformacionRequerida;
	}
}	
