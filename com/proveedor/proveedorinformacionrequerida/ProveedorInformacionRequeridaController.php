<?php
/**
 * Controlador para el recurso ProveedorInformacionRequerida
 */
namespace com\proveedor;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso proveedor, atiende a las peticiones que contengas /mvc/proveedor
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Proveedor
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/proveedorInformacionRequerida})
 */
class ProveedorInformacionRequeridaController
{
	/** @Resource(name=Proveedor) */
	protected $proveedor;
	/** @Resource(name=InformacionRequerida) */
	protected $informacionRequerida;
	/** @Resource(name=ProveedorInformacionRequerida) */
	protected $proveedorInformacionRequerida;
	/** @Resource(name=ProveedorInformacionRequeridaService) */
	protected $proveedorInformacionRequeridaService;
	/** @Resource(name=Sesion) */
	protected $sesion;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso proveedorInformacionRequerida/consultar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function consultarAction($proveedor = null, $idProveedor = null)
	{
		$this->logger->info("Atendiendo la peticion /proveedorInformacionRequerida/consultar");
		$proveedor = (!is_null($proveedor)) ? $proveedor :  '{ "idProveedor": '.$idProveedor.'}' ;
		$this->logger->info($proveedor);
		$this->proveedor->__fromJson($proveedor);
		$response = $this->proveedorInformacionRequeridaService->consultar($this->proveedor);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso proveedorInformacionRequerida/insertar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function insertarAction($proveedor, $informacionRequerida, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /proveedorInformacionRequerida/insertar");
		$this->proveedor->__fromJson($proveedor);
		$this->informacionRequerida->__fromJson($informacionRequerida);
		$this->sesion->__fromJson($sesion);
		$response = $this->proveedorInformacionRequeridaService->insertar($this->proveedor, $this->informacionRequerida, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso proveedorInformacionRequerida/eliminar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function eliminarAction($proveedor, $informacionRequerida, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /proveedorInformacionRequerida/eliminar");
		$this->proveedor->__fromJson($proveedor);
		$this->informacionRequerida->__fromJson($informacionRequerida);
		$this->sesion->__fromJson($sesion);
		$response = $this->proveedorInformacionRequeridaService->eliminar($this->proveedor, $this->informacionRequerida, $this->sesion);
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso proveedorInformacionRequerida/listar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function listarAction($proveedor)
	{
		$this->logger->info("Atendiendo la peticion /proveedorInformacionRequerida/listar");
		$this->proveedor->__fromJson($proveedor);
		$this->proveedorInformacionRequerida->setIdProveedor($this->proveedor->getIdProveedor());
		$response = $this->proveedorInformacionRequeridaService->listar($this->proveedorInformacionRequerida);
		$this->response($response);
	}
}
