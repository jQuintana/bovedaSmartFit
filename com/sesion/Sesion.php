<?php
/**
 * Modelo del recurso Sesion
 */
namespace com\sesion;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo del recurso Sesion
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Model
 * @package 	Boveda
 * @subpackage  Sesion
 * @version 	1.1
 *
 * @Component(name=Sesion)
 * @Prototype
 */
class Sesion extends BModel
{
	private $idSesion;
	private $token;
	private $fechaExpiracion;
	private $idUsuario;
	use TModel;

	public function getIdSesion()
	{		
		return $this->idSesion;
	}
	
	public function setIdSesion($idSesion)
	{
		$this->idSesion = $idSesion;
	}
	
	public function getToken()
	{
		return $this->token;
	}
	
	public function setToken($token)
	{
		$this->token = $token;
	}
	
	public function getFechaExpiracion()
	{
		return $this->fechaExpiracion;
	}
	
	public function setFechaExpiracion($fechaExpiracion)
	{
		$this->fechaExpiracion = $fechaExpiracion;
	}
	
	public function getIdUsuario()
	{
		return $this->idUsuario;
	}
	
	public function setIdUsuario($idUsuario)
	{
		$this->idUsuario = $idUsuario;
	}
}
