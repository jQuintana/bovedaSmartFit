<?php
/**
 * Controller para el recurso Usuario
 */
namespace com\mail;

use MNIComponents\Base\TController;


/**
 * Controller del recurso mail, atiende a las peticiones que contengas /mvc/mail
 *
 * @author 		Israel Hern�ndez
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Mail
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/mail})
 */
class MailController
{
	/** @Resource(name=Mail) */ 
	protected $mail;
	/** @Resource(name=MailService) */ 
	protected $mailService;
	protected $logger;
	use TController;

	/**
	 * Atiende peticiones al recurso mail/enviar
	 * @return string Cadena Json que transporta la informaci�n a la capa de la vista
	 */
	public function enviarAction($to)
	{
		$this->logger->info("Atendiendo la peticion /mail/enviar");
		$this->mail->__fromJson($to);
		$response = $this->mailService->sendMail($this->mail->getTo(), $this->mail->getSubject(), $this->mail->getText(), $this->mail->getHtml());
		$this->response($response);
	}
}
