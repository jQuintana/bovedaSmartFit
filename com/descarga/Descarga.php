<?php
/**
 * Modelo de la clase Descarga
 */
namespace com\descarga;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Descarga
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com|>
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Dscarga
 * @version 	1.0
 * 
 * @Component(name=Descarga)
 * @Prototype
 */
class Descarga extends BModel
{
	private $nombre;
	private $peticion;
	private $idFactura;
	private $idEmpresa;
	use TModel;

	public function getNombre()
	{
		return $this->nombre ;
	}
	
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}
	
	public function getPeticion()
	{
		return $this->peticion ;
	}
	
	public function setPeticion($peticion)
	{
		$this->peticion = $peticion;
	}
	
	public function getIdFactura()
	{
		return $this->idFactura ;
	}
	
	public function setIdFactura($idFactura)
	{
		$this->idFactura = $idFactura;
	}
	
	public function getIdEmpresa()
	{
		return $this->idEmpresa ;
	}
	
	public function setIdEmpresa($idEmpresa)
	{
		$this->idEmpresa = $idEmpresa;
	}
}
