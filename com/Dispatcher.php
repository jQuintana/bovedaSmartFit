<?php
/**
 * Este archivo es el encargado de orquestar la configuracion inicial
 * de la aplicación asi como frameworks y librerias que seran utilizadas 
 *
 * @author		Israel Hernández <iaejean@hotmail>
 * @category	bootstrap
 * @package		Boveda	 
 * @version	 	1.1
 */
error_reporting(E_ALL);
ini_set('display_errors', 1) ;
require_once("../../../dependencies/vendor/autoload.php");

use Ding\Autoloader\Autoloader,
	Ding\Container\Impl\ContainerImpl,
	Ding\Mvc\Http\HttpFrontController;

/**
 * Configuracion de los contenedores, subcomponentes, beans, properties, cache, etc... de @Ding
 */
$properties = array(
	'ding' => array(
		'log4php.properties' => __DIR__.DIRECTORY_SEPARATOR.'log4php.properties',
		'factory' => array(
			'bdef' => array(
				'xml' => array('filename' => "beans.xml"),
				'annotation' => array(
					'scanDir' => array(
						realpath(__DIR__),						
						realpath(__DIR__."/../../../dependencies/vendor/masnegocio/MNIComponents/src/MNIComponents/Conexion"),
						realpath(__DIR__."/../../../dependencies/vendor/masnegocio/MNIComponents/src/MNIComponents/SQLMapper"),
						realpath(__DIR__."/../../../dependencies/vendor/masnegocio/MNIComponents/src/MNIComponents/Error"),
					)
				)
			),
			'properties' => array(
				'conf.directory' 		=> __DIR__,
				'conf.directory.tmp' 	=> dirname(__DIR__).'/tmp/',
				'conf.profile' 			=> "dev" //profile[dev] = desarrollo, test, qa;  profile[pr] = produccion;
			)
		)/*,
		'cache' => array(
			'proxy' 		=> array('impl' => 'apc'),
			'bdef' 			=> array('impl' => 'apc'),
			'beans' 		=> array('impl' => 'apc'),
			'annotations' 	=> array('impl' => 'apc')
		)*/
	)
);

Autoloader::register();
HttpFrontController::handle($properties, '/smartfit/boveda/mvc');
