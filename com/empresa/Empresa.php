<?php
/**
 * Modelo de la clase Empresa
 */
namespace com\empresa;

use MNIComponents\Base\BModel;
use MNIComponents\Base\TModel;


/**
 * Modelo de la clase Empresa
 *
 * @author 		Israel Hern�ndez
 * @category	Model
 * @package 	Boveda
 * @subpackage 	Empresa
 * @version 	1.1
 *
 * @Component(name=Empresa)
 * @Prototype
 */
class Empresa extends BModel
{
	private $idEmpresa;
	private $nombre;
	private $rfc;
	private $estatus;
	private $logo;
	use TModel;

	public function getIdEmpresa()
	{
		return $this->idEmpresa ;
	}
	
	public function setIdEmpresa($idEmpresa)
	{
		$this->idEmpresa = $idEmpresa;
	}
	
	public function getNombre()
	{
		return $this->nombre ;
	}
	
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}
	
	public function getRfc()
	{
		return $this->rfc ;
	}
	
	public function setRfc($rfc)
	{
		$this->rfc = $rfc;
	}
	
	public function getEstatus()
	{
		return $this->estatus ;
	}
	
	public function setEstatus($estatus)
	{
		$this->estatus = $estatus;
	}
	
	public function getLogo()
	{
		return $this->logo ;
	}
	
	public function setLogo($logo)
	{
		$this->logo = $logo;
	}	
}
