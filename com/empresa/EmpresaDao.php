<?php
/**
 * DAO's para el recurso Empresa
 */
namespace com\empresa;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;


/**
 * DAO's para el recurso Empresa
 *
 * @author 		Israel Hernándex <iaejean@hotmail.com>
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	Empresa
 * @version 	1.1
 *
 * @Component(name=EmpresaDao)
 * @Singleton
 */
class EmpresaDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;	
	use TDao;
}
