<?php
/**
 * Cliente Webservice del validador de XML
 */
namespace com\webservice;

use MNIComponents\Base\TService;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Exception;


/**
 * Cliente Webservices del validador de XML
 *
 * @author 		Israel Hern?ndez
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Webservice
 * @version 	1.1
 * 
 * @Component(name=RequestValidadorService)
 * @Singleton
 */
class RequestValidadorService
{
	/** @Resource(name=RequestValidador) */
	protected $requestValidador;
	protected $logger;
	use TService;

	/**
	 * Este metodo realiza la petici?n al webservice de validacion de XML
	 * @param string $xml
	 * @return string
	 */
	public function request($xml, $cont = 1)
	{
		try{

			$char  = substr($xml, 0, 3);
			if($char == "\xEF\xBB\xBF"){
				$this->logger->info("Se cambia este XML");
				$xml = substr($xml, 3);

			}
			$xml = base64_encode(trim($xml));
			$headers = ['Content-Type' => 'application/json', 'Token' => $this->requestValidador->getToken()];		
			$body = ['contenidoBase64' => $xml];

			$client = new GuzzleClient();
			$res = $client->post($this->requestValidador->getWsdl(), [
				'headers' => $headers, 
				'body' => json_encode($body),
				'verify' => false
			]);
			$this->logger->info($res->getBody()->__toString());
			$response = new ResponseValidador();
			$response->setCodigo($res->getStatusCode());
			$response->setMensaje($res->getBody()->__toString());
			return $response;
		}catch(ClientException $ce){
			$response = new ResponseValidador();
			$res = $ce->getResponse();
			$response->setCodigo($res->getStatusCode());
			$response->setMensaje($res->getBody()->__toString());
			return $response;
		}catch(Exception $e){
			$this->logger->error($e->getMessage());
			$this->logger->error("Fallo en el intento # $cont");
			sleep($cont);
			if($cont == $this->requestValidador->getAttempts())  return $e->getMessage();
			$cont++;
			$this->request($xml, $cont);						
		}
	}
}
