<?php
/**
 * Controlador para el recurso Factura
 */
namespace com\webservice;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso factura, atiende a las peticiones que contengas /mvc/factura
 *
 * @author 		Israel Hern�ndez
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Factura
 * @version 	1.1
 *
 * @Controller
 * @RequestMapping(url={/resttest})
 * @Singleton
 */
class RequestValidadorController
{
	/** @resource(name=RequestValidadorService) */
	protected $requestValidadorService;
	protected $logger;
	use TController;
	
	public function mainAction($xml)
	{
		$this->logger->info("Atendiendo la peticion /factura/subir");
		$response = $this->requestValidadorService->request($xml);
		$this->response($response);
	}
}
