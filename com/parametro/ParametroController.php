<?php
/**
 * Controlador para el recurso Parametros
 */
namespace com\parametro;

use MNIComponents\Base\TController;


/**
 * Controlador del recurso Parametro, atiende a las peticiones que contengas /mvc/parametro
 *
 * @author 		Israel Hernández
 * @category	Controller
 * @package 	Boveda
 * @subpackage 	Parametro
 * @version 	1.1
 *
 * @Controller
 * @Singleton
 * @RequestMapping(url={/parametro})
 */
class ParametroController
{
	/** @Resource(name=Parametro) */
	protected $parametro;
	/** @Resource(name=ParametroService) */
	protected $parametroService;
	/** @Resource(name=Sesion) */
	protected $sesion;
	protected $logger;	
	use TController;

	/**
	 * Atiende peticiones al recurso parametro/listar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function listarAction()
	{
		$this->logger->info("Atendiendo la peticion /parametro/listar");		
		$response = $this->parametroService->listar($this->parametro);
		$this->response($response);		
	}

	/**
	 * Atiende peticiones al recurso parametro/consultar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function consultarAction($parametro)
	{
		$this->logger->info("Atendiendo la peticion /parametro/consultar");
		$this->parametro->__fromJson($parametro);
		$response = $this->parametroService->consultar($this->parametro);			
		$this->response($response);
	}
	
	/**
	 * Atiende peticiones al recurso parametro/validarEP
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 * @throws Exception si el valor del parametro EP es distinto de 1
	 */
	public function validarEPAction()
	{
		$this->logger->info("Atendiendo la peticion /parametro/validarEP");
		$response = $this->parametroService->validarEP();
		$this->response($response);
	}

	/**
	 * Atiende peticiones al recurso parametro/actualizar
	 * @return string Cadena Json que transporta la información a la capa de la vista
	 */
	public function actualizarAction($parametro, $sesion)
	{
		$this->logger->info("Atendiendo la peticion /parametro/actualizar");
		$this->parametro->__fromJson($parametro);
		$this->sesion->__fromJson($sesion);
		$response = $this->parametroService->actualizar($this->parametro, $this->sesion);
		$this->response($response);
	}
}
