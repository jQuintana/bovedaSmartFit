<?php
/**
 * Servicios del recurso Parametro
 */
namespace com\parametro;

use MNIComponents\Base\TService;
use Exception;

/**
 * Servicios del recurso Parametro
 * 
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Parametro
 * @version 	1.1
 * 
 * @Component(name=ParametroService)
 * @Singleton
 */
class ParametroService
{
	/** @Resource(name=ParametroDao) */
	protected $parametroDao;
	/** Resource(name=SesionService) */
	protected $sesionService;
	/** @Resource(name=ACLService) */
	protected $aclService;
	/** @Resource(name=Usuario) */
	protected $usuario;
	protected $logger;	
	use TService;

	/**
	 * Obtiene un objeto parametro a partir del codigo de negocio del parametro
	 * @param \com\parametro\Parametro $parametro
	 * @return \com\parametro\Parametro
	 */
	public function consultar($parametro)
	{
		$list = $this->parametroDao->consultar($parametro);
		$parametroArray  = $list[0];
		$parametro->__fromArray($parametroArray);			
		return $parametro;
	}
	
	/**
	 * Valida el parametro EP 	 
	 * @return \com\parametro\Parametro
	 * @throws Exception en caso de que el parametro sea distinto de 1
	 */
	public function validarEP()
	{
		$parametro = new Parametro();
		$parametro->setParametro("EP");
		
		$list = $this->parametroDao->consultar($parametro);
		$parametro->__fromArray($list[0]);			
		if($parametro->getValor() != 1)
			throw new Exception('Servicio Desactivado temporalmente, lamentamos las molestias Causadas, favor de volver mas tarde. <script>$("input").attr("readonly", true);</script>', 10001);
		return $parametro;
	}

	/**
	 * Obtiene un objeto parametro a partir del codigo de negocio del parametro
	 * @param \com\parametro\Parametro $parametro
	 * @param \com\jqgridfilter\JQGridFilter $jqgridfilter
	 * @return array
	 */
	public function listar(Parametro $parametro)
	{
		return $this->parametroDao->listar($parametro);
	}

	/**
	 * Obtiene un objeto parametro a partir del codigo de negocio del parametro
	 * @param \com\parametro\Parametro $parametro
	 * @param \com\sesion\Sesion $sesion
	 * @return array
	 */
	public function actualizar(Parametro $parametro, $sesion)
	{
		if($this->aclService->isAllowed($sesion, __METHOD__)){
			$parametro->setUsuarioModificacion($sesion->getIdUsuario());
			$this->parametroDao->actualizar($parametro);
			//return $this->sesionService->token($sesion);
		}
	}
}
