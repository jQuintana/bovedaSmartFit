<?php
/**
 * Servicios del recurso factura
 */
namespace com\factura;

use MNIComponents\Base\TService;
use	com\objectStorage\Upload\Upload;
use	com\factura\Factura;
use	com\mail\MailException;
use	com\objectstorage\ObjectStorageException;
use Exception;


/**
 * Servicios del recurso Factura
 * 
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @category	Service
 * @package 	Boveda
 * @subpackage 	Factura
 * @version 	1.1
 *
 * @Component(name=FacturaService)
 * @Singleton 
 */
class FacturaService
{
	/** @Resource(name=Factura) */
	protected $facturaAuxiliar;
	/** @Resource(name=FacturaDao) */
	protected $facturaDao;
	/** @Resource(name=Empresa) */
	protected $empresa;
	/** @Resource(name=EmpresaService) */
	protected $empresaService;
	/** @Resource(name=Proveedor) */
	protected $proveedor;
	/** @Resource(name=Parametro) */
	protected $parametro;
	/** @Resource(name=ParametroService) */
	protected $parametroService;
	/** @Resource(name=Bitacora) */
	protected $bitacora;
	/** @Resource(name=ProveedorService) */
	protected $proveedorService;
	/** @Resource(name=RequestValidadorService) */
	protected $requestValidadorService;
	/** @Resource(name=MailService) */
	protected $mailService;
	/** @Resource(name=ProveedorInformacionRequeridaService) */
	protected $proveedorInformacionRequeridaService;
	/** @Resource(name=BitacoraService) */
	protected $bitacoraService;
	/** @Resource(name=Upload) */
	protected $upload;
	/** @Resource(name=UploadService) */
	protected $uploadService;
	/** @Resource(name=ObjectStorage) */
	protected $objectStorage;
	/** @Resource(name=ObjectStorageService) */
	protected $objectStorageService;

	const RUTA_UPLOADS = "../uploads/";
	const COMPROBANTES = "comprobante";
	const CLIENTE      = "MARTI";
	protected $mensaje = "";
	protected $logger;
	use TService;		

	/**
	 * Servicio de validacion de una factura subida, valida que la empresa sea correcta, que el 
	 * proveedor este dado de alta y el xml por medio de un WerService
	 * @param \com\factura\Factura $factura
	 * @param $_FILE $files
	 * @return string
	 */
	public function validar($factura, $files)
	{
		foreach($files as $key => $val){			
			$this->upload->__empty();
			$this->upload->__fromArray($val);
			if($this->uploadService->validarUpload($this->upload, $key)){
				$set  = "set".ucfirst($key);
				$archivoXML = ($key == "xml") ? $this->upload->getName() : "" ;
				$factura->$set($this->upload->getTmp_name());				
			}	
		}
		$contenidoXml = file_get_contents($factura->getXml());
		$responseValidador = $this->requestValidadorService->request($contenidoXml);
		$response = $responseValidador->getMensaje();
		$this->logger->info($responseValidador);

		$this->bitacora->setUsuario("Sistema");
		$this->bitacora->setEvento($archivoXML);
		$this->bitacora->setMensaje($response);
		$this->bitacoraService->anotaBitacora($this->bitacora);
		
		$response = json_decode($response);

		if(is_object($response) || is_array($response)){
			if($responseValidador->getCodigo() == 200 ){
				if($response->respuesta == true){
					$this->empresa->setRfc($response->receptor->rfc);
					$listEmpresa = $this->empresaService->consultar($this->empresa);
					if(empty($listEmpresa))
						$this->mensaje.= date("Y-m-d H:i:s")." : Empresa no dada de alta \n";
					else{
						$this->empresa->__fromArray($listEmpresa[0]);
						$this->proveedor->setIdProveedor($factura->getIdProveedor());
						$listProveedor = $this->proveedorService->consultar($this->proveedor);					
						if(empty($listProveedor))
							$this->mensaje.= date("Y-m-d H:i:s")." : Proveedor no existe \n";
						else{												
							$this->proveedor->__fromArray($listProveedor[0]);
							if($this->proveedor->getRfc() != $response->emisor->rfc)
								$this->mensaje.= date("Y-m-d H:i:s")." : Proveedor no corresponde con el emisor del CFDI \n";
							else{
								$diasGracia = $this->validarDiasGracia($response);
								if(!$diasGracia)
									$this->mensaje.= date("Y-m-d H:i:s")." : Se han excedido los $diasGracia días de gracia después del cierre mensual para subir la factura. Por favor emitir nueva factura \n";																
								else{

									$factura->setIdEmpresa($this->empresa->getIdEmpresa());
									$factura->setFechaAlta(date("Y-m-d H:i:s"));
									$factura->setSerie($response->receptor->serie);
									$factura->setFolio($response->receptor->folio);
									$factura->setUuid($response->receptor->uuid);
									$factura->setFechaFactura(date("Y-m-d H:i:s", strtotime($response->receptor->fecha)));
									$factura->setTotal($response->receptor->total);
									$factura->setSubtotal(0);
									$factura->setTasaIva(0);
									$factura->setIva(0);
									$factura->setfechaModificacion(date("Y-m-d H:i:s"));
									$factura->setUsuarioModificacion("Proveedor");
									$factura->setMoneda($response->receptor->moneda);
									$factura->setEstatus("Pendiente");
									$factura->setRfcProveedor($this->proveedor->getRfc());
									$factura->setRfcEmpresa($this->empresa->getRfc());
									
									try{
										$list = $this->consultar($factura);
										if(empty($list)){
											$factura = $this->almacenarArchivo($factura, $files);
											$id = $this->insertar($factura);								
											$this->mensaje.= date("Y-m-d H:i:s")." : Proceso Exitoso, se ha creado un nuevo registro con el identificador $id \n";								
										}else{
											$this->facturaAuxiliar->__fromArray($list[0]);									
											if($this->facturaAuxiliar->getEstatus() == "Rechazada"){
												$this->eliminarArchivos($this->facturaAuxiliar);
												$factura = $this->almacenarArchivo($factura, $files);																	
												$this->mensaje.= date("Y-m-d H:i:s")." : Proceso Exitoso, se ha actualizado el registro con el identificador ".$this->facturaAuxiliar->getIdFactura()."\n";
												$factura->setIdFactura($this->facturaAuxiliar->getIdFactura());
												$this->actualizar($factura);
											}else
												$this->mensaje.= date("Y-m-d H:i:s")." : Esta factura ya se encuentra registrada con el identificador ".$this->facturaAuxiliar->getIdFactura()."\n";								
										}
									}catch(ObjectStorageException $objectStorageException){
										$this->eliminarArchivos($factura);
										$this->mensaje.= date("Y-m-d H:i:s")." : IMPORTANTE: No se pudo almacenar un archivo \n";								
									}
								}
							}
						}
					}
				}else{
					$this->mensaje.= date("Y-m-d H:i:s")." : Errores encontrados :\n";
					foreach($response->errores as $key => $val){
						$this->mensaje.=  "\t\t\t\t\t\t *  ".$val->mensaje."\n";
					}													
				}
			}else{
				$this->mensaje.= date("Y-m-d H:i:s")." : Errores encontrados :\n";
				foreach($response as $key => $val){
					if($val->clave == "2015"){
						$this->mensaje.=  "\t\t\t\t\t\t *  El XML ingresado presenta caracteres especiales que han dañado la estructura del archivo, es probable que la apertura del archivo en algún editor de texto haya ocasionado el problema. Por favor solicite a su PAC el archivo XML original y vuelva a intertar la carga. \n";
					}else{
						$this->mensaje.=  "\t\t\t\t\t\t *  ".$val->clave." ".$val->mensaje."\n";
					}
				}
			}				
		}else
			$this->mensaje.= date("Y-m-d H:i:s")." : Formato de archivo inválido, favor de intentarlo nuevamente \n";					
		
		$this->bitacora->setEvento($archivoXML);
		$this->bitacora->setMensaje(utf8_decode($this->mensaje));
		$this->bitacoraService->anotaBitacora($this->bitacora);
		try{
			$this->mailService->sendMail(
				$factura->getCorreoElectronico(),
				"Acuse de recibo de CFDI",
				$this->mensaje,
				""
			);
		}catch(Exception $exception){
			$this->mensaje.= date("Y-m-d H:i:s")." : IMPORTANTE: No se pudó realizar el envio de correo \n";		
		}
		return $this->mensaje;
	}

	public function validarDiasGracia(\stdclass $response)
	{
		$this->parametro->setParametro("DG");
		$this->parametro = $this->parametroService->consultar($this->parametro);
		$diasGracia = $this->parametro->getValor();
		
		$fechaLimite = strtotime(date("Y-m", strtotime($response->receptor->fecha)));	
		$fechaLimite = strtotime(date("Y-m", strtotime("+1 month", $fechaLimite)));								
		$fechaLimite = strtotime(date("Y-m-d", strtotime("+$diasGracia day", $fechaLimite)));		
		$fechaActual = strtotime(date("Y-m-d"));
		
		return ($fechaActual >= $fechaLimite) ? false : $diasGracia ;						
	}

	public function generarNombre(Factura $factura, Upload $upload)
	{
		$extension = explode(".", $upload->getName());
		$extension = end($extension);

		$nombre = "";
		if($factura->getSerie())
			$nombre.= $factura->getSerie()."_";
		$nombre.= $factura->getFolio()."_";
		$nombre.= $factura->getUuid().".";
		return $nombre.$extension;
	}


	/**
	 * Sube los archivos en la carpeta de uploads
	 * @param $_FILE $file
	 * @return string 
	 */
	public function almacenarArchivo(Factura $factura, array $files, $singlefile = false, $directorioBase = false)
	{		
		if(!$singlefile){
			foreach($files as $key => $val){
				$this->upload->__empty();				
				$this->upload->__fromArray($val);
				$this->upload->setName($key."_".$this->generarNombre($factura, $this->upload));
				$this->upload = $this->objectStorageService->uploadObject(
					$this->upload,
					self::COMPROBANTES, 			
					array(
						"cliente" 	=> self::CLIENTE,
						"anho" 		=> date("Y"),
						"mes" 		=> date("m"),
						"empresa" 	=> trim($this->empresa->getRfc())
					)
				);
				$set  = "set".ucfirst($key);
				$factura->$set($this->upload->getUrlPublica());				
			}
			return $factura;			
		}else{
			$filesAuxiliar = array();
			if($factura->getXml() != "" )
				$filesAuxiliar["xml"] = array("tmp_name" => $directorioBase.$factura->getXml(), "name" => ".xml");
			if($factura->getOrdenCompra() != "" )
				$filesAuxiliar["ordenCompra"] = array("tmp_name" => $directorioBase.$factura->getOrdenCompra(), "name" => ".pdf");
			if($factura->getAlbaran() != "" )
				$filesAuxiliar["albaran"] = array("tmp_name" => $directorioBase.$factura->getAlbaran(), "name" => ".pdf");
			if($factura->getRepresentacionGrafica() != "" )
				$filesAuxiliar["representacionGrafica"] = array("tmp_name" => $directorioBase.$factura->getRepresentacionGrafica(), "name" => ".pdf");
			foreach($filesAuxiliar as $key => $val){
				$this->upload->__empty();
				$this->upload->__fromArray($val);
				$this->upload->setName($key."_".$this->generarNombre($factura, $this->upload));
				$this->upload = $this->objectStorageService->uploadObject(
					$this->upload,
					self::COMPROBANTES, 			
					array(
						"cliente" 	=> self::CLIENTE,
						"anho" 		=> date("Y"),
						"mes" 		=> date("m"),
						"empresa" 	=> trim($this->empresa->getRfc())
					)
				);
				$set  = "set".ucfirst($key);
				$factura->$set($this->upload->getUrlPublica());				
			}
			return $factura;			
		}		
	}

	/**
	 * Servicio de validacion de una facturas subidas, valida que la empresa sea correcta, que el 
	 * proveedor este dado de alta y los xml's por medio de un WerService
	 * @param \com\factura\Factura $factura
	 * @param $_FILE $files
	 * @return string
	 */
	public function validarZip($factura, $files)
	{
		ini_set("memory_limit", "1024M");
		set_time_limit(0);

		foreach($files as $key => $val){			
			$this->upload->__empty();
			$this->upload->__fromArray($val);
			$this->uploadService->validarUpload($this->upload, $key);
		}

		$zipFile 				= self::RUTA_UPLOADS.date("Y-m-d_His")."_".mt_rand()."-".$this->upload->getName();
		$directorioZipExtraido	= self::RUTA_UPLOADS.preg_replace("/.zip/", "/", $this->upload->getName());
		move_uploaded_file($this->upload->getTmp_name(), $zipFile);
		$this->extraeDesdeZip($zipFile);
		unlink($zipFile);

		$directorioUploadHandler = opendir($directorioZipExtraido);			
		while($file = readdir($directorioUploadHandler)){
			$mensaje = "";
			if(!is_dir($directorioZipExtraido.DIRECTORY_SEPARATOR.$file)){
				$extension 		= explode(".", $file);
				$extension 		= end($extension);
				$fileRequerido 	= str_replace($extension, "", $file)."pdf";
				if($extension == "xml" || $extension == "XML"){
					$contenidoXml = file_get_contents($directorioZipExtraido.DIRECTORY_SEPARATOR.$file);
					$response = base64_decode($this->requestValidadorService->request(base64_encode($contenidoXml)));

					$this->bitacora->setUsuario("Sistema");
					$this->bitacora->setEvento(utf8_encode($file));
					$this->bitacora->setMensaje($response);
					$this->bitacoraService->anotaBitacora($this->bitacora);
					
					$response = json_decode($response);
				
					if(is_object($response)){
						if($response->response == "true"){
							$this->empresa->setRfc($response->receptor->rfc);
							$listEmpresa = $this->empresaService->consultar($this->empresa);
							if(empty($listEmpresa))
								$mensaje.= date("Y-m-d H:i:s")." : $file Empresa no dada de alta \n" ;
							else{
								$this->empresa->__fromArray($listEmpresa[0]);							
								$this->proveedor->setIdProveedor($factura->getIdProveedor());
								$listProveedor = $this->proveedorService->consultar($this->proveedor);
								if(empty($listProveedor))
									$mensaje.= date("Y-m-d H:i:s")." : $file Proveedor no existe \n" ;
								else{
									$this->proveedor->__fromArray($listProveedor[0]);
									if($this->proveedor->getRfc() != $response->emisor->rfc)
										$mensaje.= date("Y-m-d H:i:s")." : $file Proveedor no corresponde con el emisor del CFDI \n";
									else{
										$validacion = true;
										$diasGracia = $this->validarDiasGracia($response);
										if(!$diasGracia){
											$mensaje.= date("Y-m-d h:i:s")." : $file Se han excedido los $diasGracia días de gracia después del cierre mensual para subir la factura. Por favor emitir nueva factura \n";								
											$validacion = false;
										}
															
										$factura->setXml($file);
										$factura->setOrdenCompra("");
										$factura->setAlbaran("");
										$factura->setRepresentacionGrafica("");
										$factura->setIdEmpresa($this->empresa->getIdEmpresa());
										$factura->setFechaAlta(date("Y-m-d H:i:s"));
										$factura->setSerie($response->receptor->serie);
										$factura->setFolio($response->receptor->folio);
										$factura->setUuid($response->receptor->uuid);
										$factura->setFechaFactura(date("Y-m-d H:i:s", strtotime($response->receptor->fecha)));
										$factura->setTotal($response->receptor->total);
										$factura->setSubtotal(0);
										$factura->setTasaIva(0);
										$factura->setIva(0);
										$factura->setfechaModificacion(date("Y-m-d H:i:s"));
										$factura->setUsuarioModificacion("Proveedor");
										$factura->setMoneda($response->receptor->moneda);
										$factura->setEstatus("Pendiente");			
										$factura->setRfcProveedor($this->proveedor->getRfc());
										$factura->setRfcEmpresa($this->empresa->getRfc());
				
										$factura = $this->obtenPDFRequeridos($factura, $directorioZipExtraido);
									
										$mensajesInformacionRequerida = "";
										$mensajesInformacionRequerida = $this->validaRequerida($factura, $file);									
										
										if($mensajesInformacionRequerida == "" && $validacion){
											try{
												$list = $this->consultar($factura);
												if(empty($list)){
													$factura = $this->almacenarArchivo($factura, $files, $file, $directorioZipExtraido);
													$id = $this->insertar($factura);
													$mensaje.= date("Y-m-d H:i:s")." : $file Proceso Exitoso, se han creado un nuevo registro con el identificador $id y UUID ".$factura->getUuid()." \n" ;
												}else{
													$this->facturaAuxiliar->__fromArray($list[0]);
													if($this->facturaAuxiliar->getEstatus() == "Rechazada"){
														$this->eliminarArchivos($this->facturaAuxiliar);
														$factura = $this->almacenarArchivo($factura, $files, $file, $directorioZipExtraido);
														$mensaje.= date("Y-m-d H:i:s")." : $file Proceso Exitoso, se ha actualizado el registro con el identificador ".$this->facturaAuxiliar->getIdFactura()." y UUID ".$factura->getUuid()." \n" ;
														$factura->setIdFactura($this->facturaAuxiliar->getIdFactura());
														$this->actualizar($factura);
													}else
														$mensaje.= date("Y-m-d H:i:s")." : $file Esta factura ya se encuentra registrada con el identificador ".$this->facturaAuxiliar->getIdFactura()." y UUID ".$factura->getUuid()." \n" ;										
												}
											}catch(ObjectStorageException $objectStorageException){
												$this->eliminarArchivos($factura);
												$mensaje.= date("Y-m-d H:i:s")." : IMPORTANTE: $file no se pudo almacenar de manera adecuada \n" ;										
											}
										}else{
											if($mensajesInformacionRequerida != "")
												$mensaje.= date("Y-m-d H:i:s")." : $file no se encontraron los siguientes archivos complementarios : \n".$mensajesInformacionRequerida;
										}
									}
								}
							}
						}else{
							$mensaje.= date("Y-m-d H:i:s")." : $file Errores encontrados :\n";
							foreach($response->errores as $key => $val){
								$mensaje.=  "\t\t\t\t\t\t *  ".$val->mensaje."\n";
							}							
						}
					}else
						$mensaje.= date("Y-m-d H:i:s")." : $file Contiene un formato de archivo inválido, favor de verificarlo. \n";					

					$this->mensaje.= $mensaje;					
					$this->bitacora->setEvento(utf8_encode($file));
					$this->bitacora->setMensaje(utf8_decode($mensaje));					
					$this->bitacoraService->anotaBitacora($this->bitacora);
				}
			}
		}		
		closedir($directorioUploadHandler);

		$directorioUploadHandler = opendir($directorioZipExtraido);
		while($file = readdir($directorioUploadHandler)){
			if(file_exists($directorioZipExtraido.DIRECTORY_SEPARATOR.$file) && !is_dir($directorioZipExtraido.DIRECTORY_SEPARATOR.$file))
				unlink($directorioZipExtraido.DIRECTORY_SEPARATOR.$file);							
		}
		closedir($directorioUploadHandler);
		rmdir($directorioZipExtraido);
		try{
			$this->mailService->sendMail(
				$factura->getCorreoElectronico(),
				"Acuse de recibo de CFDIs",
				$this->mensaje,
				""
			);
		}catch(Exception $exception){			
			$this->mensaje.= date("Y-m-d H:i:s")." : IMPORTANTE: No se pudó enviar el correo. \n";	;
		}
		return $this->mensaje;
	}

	/**
	 * Obtiene un objeto parametro a partir del codigo de negocio del parametro
	 * @param Parametro $parametro
	 * @return Parametro
	 */
	public function validaRequerida($factura)
	{
		$mensaje = "";
		$this->proveedor->setIdProveedor($factura->getIdProveedor());
		$list = $this->proveedorInformacionRequeridaService->consultar($this->proveedor);
		foreach($list as $key => $val){
			$requerido = "get".ucfirst($val["valor"]);
			if($factura->$requerido() == "")
				$mensaje.= "\t\t\t\t\t\t * Falta ".ucfirst($val["valor"])."\n";
		}
		return $mensaje;
	}

	/**
	 * De acuerdo al servicio de consultarProveedorInformacionRequerida, se buscan los archivos 
	 * requeridos de la factura solicitada de acuerdo a cierta nomenclatura
	 * @param \com\factura\Factura $factura
	 * @return \com\factura\Factura
	 */
	public function obtenPDFRequeridos(Factura $factura, $directorioZipExtraido)
	{
		$this->proveedor->setIdProveedor($factura->getIdProveedor());
		$list = $this->proveedorInformacionRequeridaService->consultar($this->proveedor);
		foreach($list as $key => $val){
			if($val["valor"] != "xml"){
				$requerido = "set".ucfirst($val["valor"]);
				switch($val["valor"]){
					case "ordenCompra": $prefix = "oc_"; break;
					case "albaran": $prefix = "al_"; break;
					case "representacionGrafica": $prefix = "rg_"; break;					
				}					
				
				$file = explode(".", $factura->getXml());
				unset($file[count($file)-1]);
				$aux = "";
				foreach($file as $key)
					$aux.= $key;
				$fileRequerido = $prefix.$aux.".pdf";
				
				if(file_exists($directorioZipExtraido.DIRECTORY_SEPARATOR.$fileRequerido))
					$factura->$requerido($fileRequerido);				
			}
		}		
		return $factura;
	}

	/**
	 * Servicio de extracion de un zip
	 * @param string $zipName
	 * @return void
	 */
	public function extraeDesdeZip($zipFile)
	{
		$zipArchive = new \ZipArchive();
		if($zipArchive->open($zipFile) === true){
			$zipArchive->extractTo(self::RUTA_UPLOADS);
			$zipArchive->close();
		}else{
			unlink($zipFile);
			throw new Exception("El archivo no se pudó abrir, puede que no sea un ZIP o este dañado, favor de verificarlo");
		}
	}

	/**
	 * Servicio de insercion de facturas
	 * @param \com\factura\Factura $factura
	 * @return boolean
	 */
	public function insertar($factura)
	{
		return $this->facturaDao->insertar($factura);		
	}

	/**
	 * Servicio de acutalizacion de facturas
	 * @param \com\factura\Factura $factura
	 * @return boolean
	 */
	public function actualizar($factura)
	{
		return $this->facturaDao->actualizar($factura);		
	}

	/**
	 * Obtiene un listado de facturas
	 * @param Factura $factura	 
	 * @return array
	 */
	public function listarEn(array $idEmpresas)
	{	
		$listFacturas = [];
		try{
			foreach($idEmpresas as $id){
				$factura = new Factura();
				$factura->setIdEmpresa($id);
				array_push($listFacturas, $factura);			
			}		
			return $this->facturaDao->listarEn($listFacturas);
		}catch(Exception $e){
			$this->logger->info($e);
			return [];
		}
	}

	/**
	 * Elimina los archivos que tiene como referencia un objeto factura
	 * @param \com\factura\Factura $factura
	 * @return void
	 */
	public function eliminarArchivos($factura)
	{
		if($factura->getXml() != "")
			$this->objectStorageService->deleteObject($factura->getXml());

		if($factura->getOrdenCompra() != "")
			$this->objectStorageService->deleteObject($factura->getOrdenCompra());

		if($factura->getAlbaran() != "")
			$this->objectStorageService->deleteObject($factura->getAlbaran());

		if($factura->getRepresentacionGrafica() != "")
			$this->objectStorageService->deleteObject($factura->getRepresentacionGrafica());		
	}

	/**
	 * Consulta un obejeto factura
	 * @param \com\factura\Factura $factura
	 * @return array
	 */
	public function consultar($factura)
	{
		return $this->facturaDao->consultar($factura);
	}		
}
