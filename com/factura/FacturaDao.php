<?php
/**
 * DAO's para el recurso Factura
 */
namespace com\factura;

use MNIComponents\Base\TDao;
use MNIComponents\Base\IDao;

/**
 * DAO's para el recurso Factura
 *
 * @author 		Israel Hernándex
 * @category	DAO
 * @package 	Boveda
 * @subpackage 	Factura
 * @version 	1.1
 *
 * @Component(name=FacturaDao)
 * @Singleton
 */
class FacturaDao implements IDao
{
	/** @Resource(name=SQLMapperService) */
	protected $sqlMapperService;
	protected $logger;
	use TDao;
	
	public function listarEn(array $listFacturas)
	{
		return $this->sqlMapperService->ejecutarDao($listFacturas, 'listarEn');
	} 
}
