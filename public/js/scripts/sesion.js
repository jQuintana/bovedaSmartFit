/**
 * M�dulo JavaScript, controla el evento click del formulario de la sesion
 *
 * @author 		Israel Hern�ndez <iaejean@hotmail.com>
 * @version 	1.1
 * @package		Boveda
 * @subpackage 	Sesion
 */
define([],function(){

	/**
	 * Controla el evento click del formulario de la sesion, validando que los campos de password y confirmaPassword
	 * sean iguales, si es asi, envia los datos del formulario asi como la sesion para actualizar la informacion
	 * de la sesion
	 */
	function init(){
		$('body').delegate("#button_sesion", "click", function(){

			if($("#password").val() == $("#confirmaPassword").val()){
				var dataForm = $('#usuarioForm').serializeFormJSON();
				var sesion = $.getSesionJSON();
				sesion = $.evalJSON(sesion);

				$.obtenJson("usuario/actualizar", { usuario: dataForm , sesion: sesion }, function(response){
					$.setSesion(response);
					$("#div_sesion").remove();
				});				
			}else
				$("#confirmaPassword").focus();
		});
	}

	return {
		init: init
	};
});
