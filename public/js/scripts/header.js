/**
 * Módulo JavaScript, con las funcionalidades del menú principal del Sistema
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Menu
 */
define([],function(){
	/**
	 * Genera el codigo html que crea el menu, se realiza una peticion al servidor enviando el menu solicitado y el
	 * usuario que se encuentra en el objeto sesion recibido.
	 * @param sesion el objeto sesion
	 * @param string el nombre del menu que se busca
	 * @return string codigo html del menu
	 */
	function generaMenu(sesion, menuAlias){
		$.obtenJson("menuRol/consultar", { usuario: { usuario: sesion.idUsuario } , alias : { alias: menuAlias } }, function(json){
			if(json[menuAlias] != undefined){
				var opciones = json[menuAlias].options;
				var menu = "";
				for(prop in opciones){
					if(opciones[prop].options != undefined){ 
						menu+= '<li class="dropdown '+opciones[prop]["class"]+'"><a class="dropdown-toggle" data-toggle="dropdown" href="#" >'+opciones[prop]["alias"]+'<b class="caret"></b></a>';
						menu+= '<ul class="dropdown-menu '+opciones[prop]["class"]+'">';
						for(props in opciones[prop]["options"]){
							var obj = opciones[prop]["options"][props];
							if(obj["class"] == "nav-header" || obj["class"] == "divider" )
								menu+= '<li class="'+obj["class"]+'">'+obj["alias"].replace(/-/g, " ")+'</li>';
							else
								menu+= '<li class="'+obj["class"]+'"><a id="'+obj["alias"]+'" href="'+obj["url"]+'">'+(obj["alias"].replace(/-/g, " ").replace(/Catalogo/g, "Catálogo"))+'</a></li>';
						}
						menu+= '</ul>';
						menu+= '</li>';
					}else{
						menu+= '<li class="'+opciones[prop]["class"]+'"><a id="'+opciones[prop]["alias"]+'" href="'+opciones[prop]["url"]+'">'+opciones[prop]["alias"]+'</a></li>';
					}
				}			
			}
			$("#menuPrincipal").append(menu);
		});		
	}

	/**
	 * Inicializamos los eventos que estaran controlando las solicitudes de los usuarios sobre el menu principal
	 */
	function init(){
		/**
		 * Obtenemos la sesion y mostramos el nombre del usuario como un link
		 */
		var sesion = $.getSesionJSON();
		sesion = $.evalJSON(sesion);		
		
		generaMenu(sesion, "Principal");
		$.obtenJson("usuario/consultar", { usuario: {usuario: sesion.idUsuario} , sesion: sesion }, function(response){
			var usuario = response[0];	
			$("#sesion").text(usuario.nombre+" "+usuario.apellidoPaterno);			
		});
		

		/**
		 * Configuramos los eventos ocultar sobre el contenedor del menu
		 */
		$(function(){
			$(".navbar", $(this)).delay(1000).stop().animate({
				position: "absolute",
				top:"-30px"
			}, 1000);
			$("header").hover(function(event){
				if(event.type == "mouseleave")
					$(".navbar", $(this)).stop().animate({
						position: "absolute",
						top:"-30px"
						}
					);
				else
					$(".navbar", $(this)).stop().animate({
						position: "fixed",
						top:"0px"
						}
					);
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion inicio del menu
		 */
		$("body").delegate("#Inicio","click",function(){
			$("#bari").text("boveda :: Administración de Facturas");
			require(['req/text!html/home.html','script/home'],function(home_html, home_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(home_html).show("drop", {direction: "right"}, 500);
				home_js.table();
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion bitacora de boveda del menu
		 */
		$("body").delegate("#Bitacora-de-boveda","click",function(){
			$("#bari").text("boveda :: soporte :: bitacora");
			require(['req/text!html/bitacora.html','script/bitacora'],function(bitacora_html, bitacora_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(bitacora_html).show("drop", {direction: "right"}, 500);
				bitacora_js.init();
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion Catalogo de usuario del menu
		 */
		$("body").delegate("#Catalogo-de-usuarios","click",function(){
			$("#bari").text("boveda :: catalogo :: usuarios");
			require(['req/text!html/catalogoUsuario.html','script/catalogoUsuario'],function(catalogoUsuario_html, catalogoUsuario_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(catalogoUsuario_html).show("drop", {direction: "right"}, 500);
				catalogoUsuario_js.init();
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion catalogo de empresas del menu
		 */
		$("body").delegate("#Catalogo-de-empresas","click",function(){
			$("#bari").text("boveda :: catalogo :: empresas");
			require(['req/text!html/catalogoEmpresa.html','script/catalogoEmpresa'],function(catalogoEmpresa_html, catalogoEmpresa_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(catalogoEmpresa_html).show("drop", {direction: "right"}, 500);
				catalogoEmpresa_js.init();
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion catalogo de proveedores del menu
		 */
		$("body").delegate("#Catalogo-de-proveedores","click",function(){
			$("#bari").text("boveda :: catalogo :: proveedores");
			require(['req/text!html/catalogoProveedor.html','script/catalogoProveedor'],function(catalogoProveedor_html, catalogoProveedor_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(catalogoProveedor_html).show("drop", {direction: "right"}, 500);
				catalogoProveedor_js.init();
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion catalogo de parametros del menu
		 */
		$("body").delegate("#Catalogo-de-parametros","click",function(){
			$("#bari").text("boveda :: catalogo :: parametros");
			require(['req/text!html/catalogoParametro.html','script/catalogoParametro'],function(catalogoParametro_html, catalogoParametro_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(catalogoParametro_html).show("drop", {direction: "right"}, 500);
				catalogoParametro_js.init();
			});
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion indice de ayuda del menu
		 */
		$("body").delegate("#Indice-de-ayuda","click",function(){
			$("#bari").text("boveda :: ayuda");
			require(['req/text!html/ayuda.html', 'script/ayuda'],function(ayuda_html, ayuda_js){
				$("#main").hide("drop", {direction: "left"}, 500);
				$("#main").html(ayuda_html).show("drop", {direction: "right"}, 500);
				ayuda_js.init();
			});
		});

		/**
		 * Controlamos que html mostrar al dar click sobre la opcion acerca de boveda del menu
		 */
		$("body").delegate("#Acerca-de-boveda","click",function(){
			require(['req/text!html/acerca.html'],function(acerca_html){
				$(function() {
					$(acerca_html).dialog({
						autoOpen: true,
						resizable: true,
						title: "Acerca de boveda",
						modal: true,
						width: 250,
						show: { effect: "fade", duration: 500 },
						hide: { effect: "fade", duration: 500 },
						buttons: {
							Ok: function() {
								$( this ).dialog( "close" );
							}
						},
						close: function(event, ui){ $(this).remove(); },
						open: function(event, ui){ $(".ui-widget-overlay").css({ "z-index": 1030}); }
					});
				});
			});
		});

		/**
		 * Limpiamos el objeto localStorage y enviamos una peticion para eliminar la sesion, y redireccionamos
		 * la aplicacion a la pantalla de login al dar click sobre la opcion Salir del menu
		 */
		$("body").delegate("#Salir", "click", function(){			
			$.obtenJson("usuario/logout", { usuario: { usuario: $.localStorage.getItem("usuario")} }, function(response){
				$.localStorage.clear();
				location.href = "";	
			});			
		});

		/**
		 * Controlamos que html mostrar y que script cargar al dar click sobre la opcion sesion del menu, mostrando la informacion
		 * del usuario en sesion para mostrar el formulario
		 */
		$("body").delegate("#sesion","click",function(){
			$("#bari").text("boveda :: sesion");
			require(['req/text!html/sesion.html', 'script/sesion'],function(sesion_html, sesion_js){
				$(sesion_html).dialog({
					autoOpen: true,
					resizable: true,
					title: "Sesión",
					modal: true,
					width: 450,
					show: { effect: "fade", duration: 500 },
					hide: { effect: "fade", duration: 500 },
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
						}
					},
					close: function(event, ui){ $(this).remove(); },
					open: function(event, ui){ $(".ui-widget-overlay").css({ "z-index": 1030}); }
				});

				var sesion = $.getSesionJSON();
				sesion = $.evalJSON(sesion);
				
				$.obtenJson("usuario/consultar", { usuario: { usuario: sesion.idUsuario } }, function(usuario){
					usuario = usuario[0];
					for(prop in usuario){
						$("#"+prop, $('#usuarioForm')).val(usuario[prop]);
					}
					sesion_js.init();
				});
			});
		});		
		return true;		
	}
	
	/**
	 * Permitimos que las URL's mientras que se este cargando una sesion en el navegador nos puedan redireccionar al submeno dentro
	 * del menu principal de acuerdo al hashtag indicado en la URL
	 */	
	function validaHash(){	
		var valido = false;
		var hash = location.hash.replace("#","");
		if(hash != ""){
			$("a",("#menuPrincipal")).each(function(){			
				if($(this).attr("href") == "#"+hash){
					$("#"+$(this).attr("id")).trigger("click");
					valido = true;					
				}
			});		
		}
		if(!valido)		
			$("#Inicio").trigger("click");		
	}

	return {
		init: init,
		validaHash: validaHash
	};
});
