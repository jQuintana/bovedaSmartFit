/**
 * Módulo JavaScript, controla el portal de clientes
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Portal
 */
define([],function(){
	
	function obtenParametroSistema(codigo){
		return $.obtenJson("parametro/consultar", { parametro: { parametro: codigo } });		
	}

	/**
	 * Valida todos los inputs dentro de un formulario, en caso de que haya un campo vacio, se mostrara un dialog
	 * que solicitara se llene la información del formulario
	 */
	function validaCampos(formulario){
		var valido = true;
		var msg1 = "";
		var msg2 = "";
		
		var emailReg =  /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if(!emailReg.test($("#correoElectronico", $("#"+formulario)).val())){
			msg1 = "Dirección de correo inválida, favor de verificar: <br /> - Espacios en blanco, <br /> - Caracteres inválidos, por ejemplo: '.' ó ','";
			valido = false;
		}

		$('input', $("#"+formulario)).each(function(){
			if($(this).val() == ""){
				msg2 = "Favor de completar el formulario";
				valido = false;				
			}	
		});
		
		if(!valido){
			$('<div id="dialog-confirm" title="Alerta">' +
			       	'<p class="Left" style="text-align:justify;"><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 0 0;"></span>'+
			       	msg2 + '<br />' + msg1 +'.</p>' +
			       	'</div>').dialog({
				autoOpen	: true,
				resizable	: true,
				modal		: true,
				width		: 250,
				show		: {	effect	: "fade", duration: 500 },
				hide		: { effect	: "fade", duration: 500},
				buttons		: { Ok : function(){ $( this ).dialog( "close" ); } }
			});			
		}
		return valido;
	}
	
	/**
	 * Genera una nota del tipo a partir del parametro, realiza una peticion para consultar la informacion
	 * requerida por un proveedor, a partir de esa respuesta genera un el html para cmostrar una nota de los campos requeridos para un proveedor
	 *
	 * @param string id del proveedor del cual se requiere la informacion requerida
	 * @return string html de la nota de la informacion requerida
	 */
	function obtenInformacionRequeridaNota(idProveedor){
		$.obtenJson("proveedorInformacionRequerida/consultar", { idProveedor: idProveedor }, function(response){
			var nota = "<strong>NOTA:</strong> los archivos que debera incluir el ZIP son: <ul>";
			var notaBody = "";
			for(prop in response){
				var extension = (response[prop].valor == "xml") ? "text/xml" : "application/pdf" ;
				var etiqueta = "";
				switch(response[prop].valor){
					case "xml":
						etiqueta = "XML"; break;
					case "albaran":
						etiqueta = "Albaran"; break;
					case "representacionGrafica":
						etiqueta = "Representación Gráfica"; break;
					case "ordenCompra":
						etiqueta = "Orden de Compra"; break;
				}
				notaBody+= '<li>'+etiqueta+'</li>';
			}
			if(notaBody == ""){
				$("#enviar", $("#formMultiple")).fadeOut();
				$("#formInformacionRequerida", $("#formMultiple")).fadeOut();
			}else{
				nota+=notaBody+"</ul>";
				$("#formInformacionRequerida", $("#formMultiple")).html(nota).hide().fadeIn();
				$("#enviar", $("#formMultiple")).fadeIn();
			}
		});
	}
	
	/**
	 * Genera campos del tipo input>file apartir del parametro, realiza una peticion para consultar la informacion
	 * requerida por un proveedor, a partir de esa respuesta genera un el html para crear los inputs
	 *
	 * @param string id del proveedor del cual se requiere la informacion requerida
	 * @return string html de los inputs de la informacion requerida
	 */
	function obtenInformacionRequerida(idProveedor){
		$.obtenJson("proveedorInformacionRequerida/consultar", { idProveedor: idProveedor}, function(response){
			var inputs = "";
			for(prop in response){
				var extension = (response[prop].valor == "xml") ? "text/xml" : "application/pdf" ;
				var etiqueta = "";
				switch(response[prop].valor){
					case "xml":
						etiqueta = "XML"; break;
					case "albaran":
						etiqueta = "Albaran"; break;
					case "representacionGrafica":
						etiqueta = "Representación Gráfica"; break;
					case "ordenCompra":
						etiqueta = "Orden de Compra"; break;
				}
				inputs+= '<div class="form-group">'+
							'<label class="col-sm-2 control-label" for="'+response[prop].valor+'">'+etiqueta+': </label>'+
							'<input class="form-control input-small" type="file" name="'+response[prop].valor+'" id="'+response[prop].valor+'" accept="'+extension+'"/>'+
						'</div>';
			}
			if(inputs == ""){
				$("#enviar", $("#formUnitaria")).fadeOut();
				$("#formInformacionRequerida", $("#formUnitaria")).fadeOut();
			}else{
				$("#formInformacionRequerida", $("#formUnitaria")).html(inputs).hide().fadeIn();
				$("#enviar", $("#formUnitaria")).fadeIn();
				
				if(!(navigator.appVersion.indexOf("MSIE") != -1)){ //FIX IE8
					$(':file').customFileInput({
						button_position 	: 'right',
						feedback_text		: 'Cargar archivo...',
						button_text			: 'Archivo',
						button_change_text	: 'Cambiar'
					
			    	});
			    }
			}
		});		
	}
	
	/**
	 * Inicializa los eventos que controla la pantalla del portal
	 */
	function init(){
		/**
		 * Iniciliza el nice scroll 
		 */
		$("#div_resultados").niceScroll();
		/**
		 * Inicializa la funcionalidad de las pestañas de bootstrap
		 */
		$('#tabs a').click(function (e) {
			e.preventDefault()
			$(this).tab('show')
		});

		/**
		 * Inicializa la funcionalidad de los campos tipo file
		 */
		$("#correoElectronico").on('blur', function(){
			valido = true;
			var emailReg =  /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

			if(!emailReg.test($("#correoElectronico").val())){
				msg1 = "Dirección de correo inválida, favor de verificar: <br /> - Espacios en blanco, <br /> - Caracteres inválidos, por ejemplo: '.' ó ','";
				valido = false;
			}

			if(!valido){
				$('<div id="dialog-confirm" title="Alerta">' +
			       	'<p class="Left" style="text-align:justify;"><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 0 0;"></span>' +
			       	msg1 +'.</p>' +
			       	'</div>').dialog({
					autoOpen	: true,
					resizable	: true,
					modal		: true,
					width		: 250,
					show		: {	effect	: "fade", duration: 500 },
					hide		: { effect	: "fade", duration: 500},
					buttons		: { Ok : function(){ $( this ).dialog( "close" ); } }
				});
			}
			return valido;
		});

		/**
		 * Inicializa la funcionalidad de los campos tipo file
		 */
		if(!(navigator.appVersion.indexOf("MSIE") != -1)){ //FIX IE8
			$(':file').customFileInput({
				button_position 	: 'right',
				feedback_text		: 'Cargar archivo...',
				button_text			: 'Archivo',
				button_change_text	: 'Cambiar'
			});		
		}
		
		if($('html').is('.lt-ie9')){ //FIX IE8
			$('<div id="dialog-confirm" title="Alerta">' +
				'<p class="Left" style="text-align:justify;"><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 0 0;"></span>'+
				'Hemos detectado una versión de un navegador incompatible, por favor actualiza tu navegador favorito <a target="_blank" href="http://browsehappy.com/?locale=es"></span>click aquí.</a></p>'+
				'</div>').dialog({
			autoOpen	: true,
			resizable	: true,
			modal		: true,
			width		: 350,
			show		: { effect	: "fade", duration: 500 },
			hide		: { effect	: "fade", duration: 500 },
			buttons		: {
							Ok : function(){
									$("input").attr("readonly", true);
									$("button").attr("disabled", true);
									$( this ).dialog( "close" );
								}
						}
			});						
		}else{
			if(obtenParametroSistema("EP").valor == 1){
				/**
				 * Controla el evento que soolicita la informacion requerida al salir del campo idProveedor
				 */
				$("#idProveedor", $("#formUnitaria")).blur(function(){
					obtenInformacionRequerida($(this).val());					
				});
				/**
				 * Controla el evento que soolicita la informacion requerida al salir del campo idProveedor
				 */
				$("#idProveedor", $("#formMultiple")).blur(function(){
					obtenInformacionRequeridaNota($(this).val());					
				});

				/**
				 * Controla el evento que envia la informacion al servidor
				 */
				$(':button').click(function(event){				
					$("#div_resultados").val("");
					var id = $(this).parent().parent().attr("id");
					if(validaCampos(id)){
						$.blockUI({ message: '<h5>Espere un momento...</h5>', theme: true, fadeIn: 0  });
						$('html').append('<iframe id="iframe" name="iframe" style="display:none;" src=""></iframe>');
						$("#"+id).submit();
						$("#iframe").load(function(e){
							var objUploadBody = window.frames[$(this).attr("id")].document.getElementsByTagName("body")[0];
							var jBody = $(objUploadBody);										
							var response = jBody.html().replace(/(<([^>]+)>)/ig,"")
							try{
								response = $.evalJSON(response);
								if(response.code == undefined){
									$("#div_resultados").val(response);	
								}else{								
									$('<div>' +
							            '<p class="Left" style="text-align:justify;"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>' +
							            'Mensaje: ' + response.message +
							            '<br/> Fecha: ' + response.time +
										'<br/> Error #: ' + response.code +
							            '</p></div>').dialog({
							            resizable		: false,
							            modal			: true,
							            width 			: "auto",
										title			: 'Error',
							            buttons			: { "Cerrar": function(){ $(this).dialog("close"); } },
										close			: function(event, ui){ $(this).remove(); }
							        });
								}
							}catch(ex){
								console.log(ex);
							}	
							$.unblockUI();								
							$(this).remove();
							e.preventDefault();
							e.stopPropagation();
							event.preventDefault();
							event.preventDefault();									
						});	
					}
				});
			}else{
				$("input").attr("readonly", true);
				$("button").remove();
				$('<div id="dialog-confirm" title="Alerta">' +
						'<p class="Left" style="text-align:justify;"><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 0 0;"></span>'+
						'Servicio Desactivado temporalmente, lamentamos las molestias Causadas, favor de volver mas tarde.</p>' +
						'</div>').dialog({
					autoOpen	: true,
					resizable	: true,
					modal		: true,
					width		: 350,
					show		: { effect	: "fade", duration: 500 },
					hide		: { effect	: "fade", duration: 500 },
					buttons		: { Ok : function(){ $( this ).dialog( "close" ); } }
				});
				
			}
		}
	}

	return {
		init: init
	};
});