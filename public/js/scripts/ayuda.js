/**
 * Módulo JavaScript, con las funcionalidades de la pantalla del Catálogo de Parámetros del Sistema
 *
 * @author 		Israel Hernández <iaejeam@hotmail.com>
 * @version 	1.0
 * @package 	Boveda
 * @subpackage 	Ayuda
 */
define([],function(){

	/**
	 * Esta funcion se encarga de inicializar el pluging affix que permite mantener el nav-sidebar estatico en una
	 * posicion
	 */
	function init(){
		$("#ul_help").affix({
			offset: $('#ul_help').position()
		});
	}

	return {
		init: init
	};
});