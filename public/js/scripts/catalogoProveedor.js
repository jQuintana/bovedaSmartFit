/**
 * Módulo JavaScript, con las funcionalidades del catalogo de proveedores
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Proveedor
 */
define([],function(){

	/**
	 * Esta Funcion permite formatear un valor proveniente de grid creando un elace par amostrar un grid nuevo sobre
	 * la pantalla para mostrar la realcion con la informacion requerida
	 * @param string cellvalue
	 * @param Object options
	 * @param Object rowObject
	 * @return string
	 */
	function currencyFmatter (cellvalue, options, rowObject){
		return '<a href="#providerHasInformacionRequerida_'+cellvalue+'" id="'+cellvalue+'"><span class="icon-folder-open"></span></a>';
	}

	/**
	 * Esta Funcion permite formatear un valor proveniente de grid, para mostrar el estatus de los proveedores
	 * @param string cellvalue
	 * @param Object options
	 * @param Object rowObject
	 * @return string
	 */
	function estatusFormatter(cellvalue, options, rowObject){
		return (cellvalue == 1) ? "Activo" : "Inactivo";
	}

	/**
	 * Crea e inicializa el grid de proveedores
	 */
	function init(){
		/**
		 * Inicializa las variables que controlaran el grid
		 */
		var nameTable 	= "table_jsonProveedores";
		var table 		= $("#"+nameTable);
		var pager 		= "#div_pager";

		/**
		 * Carga el html, y el script sobre un dialogo al dar clic sobre uno de los iconos formateadmos para mostrar
		 * la relacion proveedor/informaicon requerida
		 */
		table.delegate("a","click",function(){
			var id = $(this).attr("id");
			require(['req/text!html/catalogoProveedorHasInformacionRequerida.html', 'script/catalogoProveedorHasInformacionRequerida'], function(catalogoProveedorHasInformacionRequerida_html, catalogoProveedorHasInformacionRequerida_js){
				$(catalogoProveedorHasInformacionRequerida_html).dialog({
					autoOpen: true,
					resizable: true,
					title: "Proveedor "+id+" Información Requerida",
					modal: true,
					width: "auto",
					height: "auto",
					show: { effect: "fade", duration: 500 },
					hide: { effect: "fade", duration: 500 },
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
						}
					},
					close: function(event, ui){ $(this).remove(); },
					open: function(event, ui){$(".ui-widget-overlay").css({ "z-index": 1030});}
				});
				catalogoProveedorHasInformacionRequerida_js.init(id);
			});
		});

		/**
		 * Crea y configura el grid de proveedores
		 */
		var mygrid = table.jqGrid({
			url: $.urlBase+'proveedor/listar',
			datatype: "json",
			colNames:['ID Proveedor','RFC','Nombre','Estatus', 'Inf. req.'],
			colModel:[
				{name:'idProveedor',index:'idProveedor', formoptions:{elmprefix:'(*) '}, key:true, editable:true, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
				{name:'rfc',index:'rfc', editable:true, formoptions:{elmprefix:'(*) '}, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
				{name:'nombre',index:'nombre', editable:true, formoptions:{elmprefix:'(*) '}, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
				{name:'estatus',index:'estatus', formatter:estatusFormatter,  formoptions:{elmprefix:'(*) '}, align:"center", editable:true, edittype:"select", editoptions: {value:"1:Activo; 0:Inactivo", dataInit: function (elem) {$(elem).css('width', "80%");}},  stype:'select', searchoptions:{sopt:['eq','ne'],value:':Todos;1:Activo;0:Inactivo'}},
				{name:'idProveedor', index: 'idProveedor', formatter:currencyFmatter, search:false, align:"center", width:50}
			],
			pager: pager,
			sortname: 'idProveedor',
			caption: "Proveedores - Boveda"
		});

		/**
		 * Confira los eventos de edicion, insercion y eliminacion de registros sobre el grid
		 */
		table.jqGrid('navGrid', pager,
			{edit:true, add:true, del:true, search: false, refresh:true },
			{
                url: $.urlBase+"proveedor/actualizar",
				beforeShowForm: function(form){
					$('#editmod'+nameTable).center();
                    $('#tr_idProveedor', form).hide();
                },
				onclickSubmit: function(rowid){
					var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
					return { proveedor: $.toJSON(dataForm), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
            },
			{
                url: $.urlBase+"proveedor/insertar",
				beforeShowForm: function(form) {
					$('#editmod'+nameTable).center();
                    var nameColumnField = $('#tr_idProveedor', form).show();
                    $('<tr class="FormData" id="tr_AddInfo"><td class="CaptionTD ui-widget-content"><b>Información adicional:</b></td></tr>').insertAfter(nameColumnField);
                },
				onclickSubmit: function(rowid){
					var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
					return { proveedor: $.toJSON(dataForm), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			},
			{
                url: $.urlBase+"proveedor/eliminar",
                beforeShowForm: function(form) {
					$('#delmod'+nameTable).center();
				},
				onclickSubmit:function(options, rowid){
					return { proveedor: $.toJSON({ idProveedor: rowid}), sesion: $.getSesionJSON() };
				},
				afterComplete: function(response){
					$.setSesion($.evalJSON(response.responseText));
				}
			}
		);

		/**
		 * Crea un boton para mostrar/ocultar el filterToolbar en el grid
		 */
		table.jqGrid('navButtonAdd', pager,{caption:"",title:"Ocultar/Mostrar barra de búsqueda", buttonicon :'ui-icon-pin-s',
			onClickButton:function(){
				mygrid[0].toggleToolbar()
			}
		});

		/**
		 * Crea un boton para limpiar la informacion ingresada el el filterToolbar del grid
		 */
		table.jqGrid('navButtonAdd', pager,{caption:"",title:"Limpiar Búsqueda",buttonicon :'ui-icon-refresh',
			onClickButton:function(){
				mygrid[0].clearToolbar()
			}
		});

		/**
		 * Configura el filterToolbar del grid
		 */
		table.jqGrid('filterToolbar');

		/**
		 * Formatea el tamaño del los inputs del paginador
		 */
		$(".ui-pg-input").addClass(" form-control input-mini");
		$(".ui-pg-selbox").addClass(" form-control input-mini");
	}

	return {
		init: init
	};
});