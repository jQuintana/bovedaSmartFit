/**
 * Módulo JavaScript, con las funcionalidades del menú catalogo de empresas
 *
 * @author 		Israel Hernández <iaejean@hotmail.com>
 * @version 	1.1
 * @package 	Boveda
 * @subpackage 	Empresa
 */
define([],function(){

	/**
	 * Esta Funcion permite formatear un valor proveniente de grid para mostrar el estatus de la empresa
	 * @param string cellvalue
	 * @param Object options
	 * @param Object rowObject
	 * @return string
	 */
	function estatusFormatter(cellvalue, options, rowObject){
		return (cellvalue == 1) ? "Activa" : "Inactiva";
	}

	/**
	 * Inicializamos los eventos que controlan la pantalla y el grid de empresas
	 */
	function init(){
		/**
		 * Inicializamos la variables que controlan el grid
		 */
		var nameTable 	= "table_jsonEmpresas";
		var table 		= $("#"+nameTable);
		var pager 		= "#div_pager";

		/**
		 * Creamos y configuramos el grid de emrpesa
		 */
		var mygrid = table.jqGrid({
			url			: $.urlBase+'empresa/listar',
			datatype	: "json",
			colNames	: ['idEmpresa','RFC','Nombre','Logo','Estatus'],
			colModel	: [
							{name:'idEmpresa',index:'idEmpresa', hidden:true, editable:true, key:true},
							{name:'rfc',index:'rfc', editable:true, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
							{name:'nombre',index:'nombre', editable:true, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}},
							{name:'logo',index:'logo', hidden:true, editable:true, editoptions: { dataInit: function (elem) {$(elem).css('width', "80%");}}},
							{name:'estatus',index:'estatus', formatter:estatusFormatter,  editable:true, edittype:"select", editoptions:{value:"1:Activa; 0:Inactiva", dataInit: function (elem) {$(elem).css('width', "80%");}}, editrules:{required:true}}
						],
			pager		: pager,
			sortname	: 'rfc',
			caption		: "Empresas - Boveda"
		});

		/**
		 * Configuramos los eventos de edicion, incersion y eliminacion de registros en el grid
		 */
		table.jqGrid('navGrid', pager,
			{edit:true, add:true, del:true, search: false, refresh:true },
			{
                url				: $.urlBase+'empresa/actualizar',
				beforeShowForm	: function(form){
									$('#editmod'+nameTable).center();
									$('#tr_rfc', form).hide();
								},
				onclickSubmit	:function(rowid){
									var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
									return { empresa: $.toJSON(dataForm), sesion: $.getSesionJSON() };
								},
				afterComplete	: function(response){
									$.setSesion($.evalJSON(response.responseText));
								}
            },
			{
				url				: $.urlBase+'empresa/insertar',
                beforeShowForm	: function(form) {
                					$('#editmod'+nameTable).center();
									var nameColumnField = $('#tr_rfc', form).show();
									$('<tr class="FormData" id="tr_AddInfo">'+
										'<td class="CaptionTD ui-widget-content"><b>Información adicional:</b></td></tr>').insertAfter(nameColumnField);
								},
                onclickSubmit	:function(rowid){
									var dataForm = $("#FrmGrid_"+nameTable).serializeFormJSON();
									return { empresa: $.toJSON(dataForm), sesion: $.getSesionJSON() };
								},
				afterComplete	: function(response){
									$.setSesion($.evalJSON(response.responseText));
								}
			},
			{
				url				: $.urlBase+'empresa/eliminar',
				beforeShowForm	: function(form) {
                					$('#delmod'+nameTable).center();
                				},
                onclickSubmit	:function(options, rowid){
                					return { empresa: $.toJSON({ idEmpresa: rowid }), sesion: $.getSesionJSON() };									
								},
				afterComplete	: function(response){
									$.setSesion($.evalJSON(response.responseText));
								}
			}
		);
	}

	return {
		init: init
	};
});