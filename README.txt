Author: Israel Hernández Valle
Version: 1.1
Project: Boveda 2.0


Arquitectura:
	comunicacion entre front y back por medio de mensajes JSON
	apache2
		Frontend:
			View
				HTML5
				CSS
					bootstrap
					fileinput
					jqgrid
					jqueryui
					boveda
				Javascript
					modernizer
					requirejs
					jquery
						jqgrid
						json
						jqueryui
						blockui
						bootstrap
						fileinput
						filedownload
						enhance
		Backend:
			MySQL
			PHP
				com
					controller
					model
					service
					dao
				dependencies
					ding
					mailgun
					log4php
					phpunit
					phpdocumentor

Requerimientos:
	* Apache2
	* PHP v5.4+
	* CURL
	* SVN
	* GIT
	* Composer
	* MySQL v5+

Instrucciones de instalación:

	Instalación de Zend Server
		Linux
			Debian											|| RHEL
			wget(http://zend.com/en/download/709)			|| wget(http://zend.com/en/download/709)
			sudo ./install.sh 5.4							|| sudo ./install.sh 5.4   
			sudo apt-get install mysql-server mysql-client	|| yum instal mysql-server mysql
			sudo apt-get install git 						|| yum install git
			sudo apt-get install subversion 				|| yum install subversion
			sudo apt-get install curl 						|| yum install curl
			curl -sS https://getcomposer.org/installer | /usr/local/zend/bin/php
			sudo mv composer.phar /usr/local/bin/composer	|| mv composer.phar /usr/local/bin/composer
		Windows
			Descargar e instalar Zend Server
			Descargar e instalar MySQL
			Descargar e instalar GIT
			Descargar e instalar Tortoise SVN
			Descargar e instalar Composer

	Descarga del proyecto:
		Linux
			cd /var/www/
			mkdir "cliente"/boveda
			sudo checkout URL_boveda "cliente"/boveda
			sudo checkout URL_dependencies dependencies
		Windows
			realizar con ayuda deyl svn tortoise una checkout sobre la ruta "cliente"/boveda en el directorio htdocs
			realizar con ayuda del svn tortoise una checkout sobre la ruta dependencies en el directorio htdocs

	Configurar apache:rm
		/etc/httpd/conf/httpd.conf
		Linux
			sudo a2enmod rewrite
		Windows & Linux
			modificar en archivo de configuracion de apache, modificar AllowOverride None por  AllowOverride All
		Linux
			Debian						 || RHEL
			sudo service apache2 restart || apachectl restart
		Windows
			reiniciar apache desde el monitor

	Composer:
		Linux
			cd /var/www/dependencies
			path_php path_composer_phar install/update
		Windows
			...\htdocs\dependencies
			composer install/update

	PHPUnit:
		Linux
			cd /var/www/"cliente"/boveda/docs/
			path_php path_phpUnit -c PHPUnit.xml
		Windows
			...\htdocs\dependencies\vendor\bin\phpunit -c PHPUnit.xml

	PHPDocumentor:
		Linux
			cd /var/www/"cliente"/boveda/test/
			path_php path_phpDoc -c PHPDocs.xml
		Windows
			...\htdocs\dependencies\vendor\bin\phpdoc.bat.php -c PHPDocs.xml

	Configuracion de la aplicación:
		"cliente"/boveda/com/beans-datasource.xml
		"cliente"/boveda/com/initphp.properties
		Log4PHP:
			"cliente"/boveda/com/log4php.properties
			"cliente"/boveda/test/log4php.properties
		PHPDocumentor
			"cliente"/boveda/docs/PHPDocs.xml
		PHPUnit
			"cliente"/boveda/test/PHPUnit.xml

	NOTA: service mysqld start 
		  mysql_secure_installation 